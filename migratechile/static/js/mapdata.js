var simplemaps_countrymap_mapdata={
  main_settings: {
    //General settings
		width: "responsive", //or 'responsive'
    background_color: "#FFFFFF",
    background_transparent: "yes",
    border_color: "#ffffff",
    pop_ups: "detect",
    
		//State defaults
		state_description: "State description",
    state_color: "#88A4BC",
    state_hover_color: "#3B729F",
    state_url: "",
    border_size: 1.5,
    all_states_inactive: "no",
    all_states_zoomable: "yes",
    
		//Location defaults
		location_description: "Location description",
    location_url: "",
    location_color: "#FF0067",
    location_opacity: 0.8,
    location_hover_opacity: 1,
    location_size: 25,
    location_type: "square",
    location_image_source: "frog.png",
    location_border_color: "#FFFFFF",
    location_border: 2,
    location_hover_border: 2.5,
    all_locations_inactive: "no",
    all_locations_hidden: "no",
    
		//Label defaults
		label_color: "#d5ddec",
    label_hover_color: "#d5ddec",
    label_size: 22,
    label_font: "Arial",
    hide_labels: "no",
    hide_eastern_labels: "no",
   
		//Zoom settings
		zoom: "yes",
    manual_zoom: "yes",
    back_image: "no",
    initial_back: "no",
    initial_zoom: "-1",
    initial_zoom_solo: "no",
    region_opacity: 1,
    region_hover_opacity: 0.6,
    zoom_out_incrementally: "yes",
    zoom_percentage: 0.99,
    zoom_time: 0.5,
    
		//Popup settings
		popup_color: "white",
    popup_opacity: 0.9,
    popup_shadow: 3,
    popup_corners: 5,
    popup_font: "12px/1.5 Verdana, Arial, Helvetica, sans-serif",
    popup_nocss: "no",
    
		//Advanced settings
		div: "map",
    auto_load: "yes",
    url_new_tab: "yes",
    images_directory: "default",
    fade_time: 0.1,
    link_text: "View Website"
  },
  state_specific: {
    CHL2691: {
      name: "Aisén del General Carlos Ibáñez del Campo",
      description: '<br><b>Fiestas Costumbristas:</b> <br>'+
      "The summer of this year more than 50 costumbristas celebrations in the region were realized. In several towns and cities there are festivals to make known the Chilean traditions, with jockeys, such as the International Festival of Jinetadas of Villa Amengual, gastronomic exhibitions such as the Fiesta del Pesca'o Frito in Puerto Cisnes, live music especially folkloric and our typical dances.",
      color: "#ffa71a",
      hover_color: "#cc7e00",
      url: ""
    },
    CHL2692: {
      name: "Magallanes y Antártica Chilena",
      description: '<br><b>Carnavales de Invierno:</b><br>'+
      'In Puerto Williams, the southernmost city in the world, takes place in July, along with the community, the Snow Festival. The same month, in Punta Arenas, there is the Winter Carnival, a traditional festival where the beginning of winter and the shortest night of the year are celebrated, during the weekend closest to July 21. It involves a large number of floats and parades that cross the streets of the city. As in most festivities, a queen is also chosen. One of the most anticipated activities is the 25 minutes of fireworks that are fired.',
      color: "#8cc059",
      hover_color: "#669438",
      url: ""
    },
    CHL2693: {
      name: "Tarapacá",
      description: '<br><b>La Tirana:</b> <br>'+
      'La Tirana is a small town in the Tarapacá Region close to the local capital, Iquique. The fiesta in honor of the Virgen del Carmen not only Tarapaqueños, but many Chileans from different sides, pilgrims and tourists. Between the 12th and the 17th of July of each year, dancers and musicians give life to the diablada, a popular religious festival, to expel the demons. The body of dance, with colorful costumes and masks, moves to the rhythm of drums and flutes. In the temple, masses are offered, while in the surroundings there are crafts, meals and dancing that does not stop throughout the day. <br><br>'+
      '<b>Dakar Rally: </b><br>'+
      'The route of the Dakar Rally, a rally competition held in January, crosses a wide variety of terrains, including the Pampa plain, the Argentine Patagonia, the Atacama desert in Chile and now extended its outline to Peru. Although this event is not national, nor does it belong to a single region, it belongs to the north. It has seven stages in our country, where competitors from Salta, make a pass through Calama, Iquique, Antofagasta, El Salvador, La Serena, to finally reach Valparaíso.',
      color: "#ff1a1a",
      hover_color: "#cc0000",
      url: ""
    },
    CHL2694: {
      name: "Arica y Parinacota",
      description: '<br>Since 2004, Arica has hosted the Bodyboard World Championship "Arica Chilean Challenger". In this tournament are the main exponents of bodyboarding, to compete for who has greater skills in the northern waves.',
      color: "#8cc059",
      hover_color: "#669438",
      url: ""
    },
    CHL2695: {
      name: "Antofagasta",
      description: '<br><b>La Tirana Chica: </b><br>'+
      'Both in Antofagasta, as in Arica, Iquique, Alto Hospicio, Tocopilla and Calama, Tirana Chica is also held annually. In its beginnings, this folkloric celebration in honor of the Virgen del Carmen, was celebrated on July 24 (eight days after the La Tirana Festival), but the date has become mobile, varying between one or two weeks after the main party. <br><br>'+
      '<b>Festival of Antofagasta:</b> <br>'+      
      'The Antofagasta Festival, is a musical contest that commemorates the anniversary of the city on February 14. It comes to replace the extinct Festival Verano Naranja de Antofagasta, which took place in the city between 2004 and 2008.',
      color: "#1cd0fd",
      hover_color: "#02a2ca",
      url: ""
    },
    CHL2696: {
      name: "Atacama",
      description: '<br><b>Saint Peter and Saint Paul:</b><br>'+
      'On June 29, fishermen, divers, shell fishers and sea workers celebrate the patron of the Catholic Church, San Pedro. The celebrations begin days before and on the 28th a mass is celebrated on the eve of midnight, giving way to dawn dances. On the 29th the great liturgical ceremony takes place, which once finished, gives way to the procession of the image of the patron, which is always marked by the color and enthusiasm of the dance groups, transforming into a true religious carnival. On the other hand, in a boat surrounded by other boats, the figure of the apostle circulates, who receives the prayers to intercede for abundant fishing, good health and a benevolent sea.',
      color: "#ffa71a",
      hover_color: "#cc7e00",
      url: ""
    },
    CHL2697: {
      name: "Coquimbo",
      description: '<br><b>The Pampilla:</b><br>'+
      'La Pampilla is a festival that takes place on September 18, 19 and 20 of each year (although it usually lasts 2 days before and after such dates) in the esplanade of the same name, located in the city of Coquimbo. During this period hundreds of families settle with tents and vehicles in the hills. The origin of this party would be related to the First Governing Board, which was known in Coquimbo only two days later, because at that time there were no great advances in communication. To celebrate this news, the inhabitants of Coquimbo congregated in the area of ​​La Pampilla, and celebrated with cuecas, tonadas and chicha. It is currently used in September as a camping area, with family activities organized by the municipality.<br><br>'+
      '<b>Virgin of Andacollo: </b><br>'+      
      'In the commune of Andacollo, in the province of Elqui, the religious festival, cataloged by many as the most important in Norte Chico, dedicated to the Virgin of Andacollo, takes place at the end of each year. Here it is honored with dances, music and special meals. The "Fiesta Chica" takes place on the first Sunday of October, while the "Fiesta Grande" takes place from December 23 to 27, when the Virgin ends her journey and returns from the New Temple to the Old. That day the doors of the main basilica are closed, the instruments are silent, the pilgrims return to their places of origin.',
      color: "#e732c9",
      hover_color: "#b7159c",
      url: ""
    },
    CHL2698: {
      name: "Región Metropolitana de Santiago",
      description: '<br> <b>Cuasimodo:</b> <br>'+
      'It takes place in the communes of Santiago, Talagante, Quilicura, Colina, Las Condes, Barnechea, Conchalí, Maipú, Isla de Maipo, Alto Jahuel, Chada. It transcends the Metropolitan Region. It is a ceremony in which communion is taken to the sick. The priest "runs to Christ" accompanied by worshipers on horseback, bicycle and on foot, dressed especially for the occasion, with their heads covered by colored scarves.<br><br>'+
      '<b>Procesión del Cristo de Mayo:</b> <br>'+
      'At sunset this traditional procession begins. From the Church of San Agustín, in the middle of prayers and religious songs, the image of Cristo de Mayo is taken to the Plaza de Armas in Santiago. This traditional festival is held since the times of the Colony.<br><br>'+
      '<b>Fiesta de la Virgen del Carmen: </b><br>'+
      'Throughout the day the pilgrims go to the Votive Temple of Maipú, where the Virgin is celebrated with songs, dances, prayers and colorful costumes.<br><br>'+
      '<b>Fiesta del Huaso:</b><br>'+
      'This peasant festival is held in honor of the Virgen del Carmen. The huasos ride their decorated horses, making a beautiful and picturesque parade.',
      color: "#8cc059",
      hover_color: "#669438",
      url: ""
    },
    CHL2699: {
      name: "Valparaíso",
      description: '<br><b>Festival de Viña del Mar:</b> <br>'+
      'The Viña del Mar International Song Festival since its inception on February 21, 1960, is considered the largest festival in Latin America and the most important in the American continent. Although the competitions of international popular music and national folklore were the reason of origin of the contest, for years they have been displaced in terms of the media importance achieved by the invited artists, who have become the real highlight of the Festival. <br><br>'+      
      '<b>Huaso de Olmué:</b> <br>'+
      'The Festival de Huaso de Olmué is a four-day musical contest. Its first edition took place in January of 1970 and it included diverse categories, all related to traditions of the Chilean countryside: payadores, better cueca cantada, better tonada, test of dexterity on horseback and fashion of the huaso. Currently has a Latin American folk competition, which is the meaning of the contest.',
      color: "#ff1a1a",
      hover_color: "#cc0000",
      url: ""
    },
    CHL2700: {
      name: "La Araucanía",
      description: '<br><b>Año Nuevo Indígena:</b><br>'+
      'The original Aymara, Quechua, Rapa Nui and Mapuche peoples have their own ancestral calendar, where the new year begins with the winter solstice and is celebrated on the night of June 24. Grafica that finished the harvest in the fields and the land must prepare for the sowing and to renew its fertility. It is a new life cycle and Aboriginal cultures are grateful to nature. This celebration is called We Tripantu, which means "new return or return of the sun" and is celebrated in rural areas of the south, as in the main square of Temuco.',
      color: "#ff1a1a",
      hover_color: "#cc0000",
      url: ""
    },
    CHL2701: {
      name: "Los Ríos",
      description: '<br><b>Festival de Cine de Valdivia:</b><br>'+
      'The Valdivia International Film Festival (FICV or FICVALDIVIA) takes place annually since its first release in 1994. It is one of the most important national and cultural events at the national level, for which nearly 5,000 film productions have been made, both national as international. Those who win the competition with their film, take the statue of a golden pudou.<br><br>'+
      '<b>Semana Valdiviana:</b><br>'+
      'To celebrate the foundation of Valdivia in February, dozens of boats give life to a river corsal. The ornate ships compete for a prize for the most glamorous and, obviously, a beauty queen is chosen. A fireworks display closes the party, while on the banks of Calle-Calle there are artistic presentations, gastronomy and beer, recalling the cultural influence of the German immigrants who populated the area since the 19th century.',
      color: "#8cc059",
      hover_color: "#669438",
      url: ""
    },
    CHL2702: {
      name: "Bío-Bío",
      description: '<br><b>San Sebastian de Yumbel:</b> <br>'+
      'Yumbel is a commune of the Biobío Region, where San Sebastián is worshiped every January 20 and March, through an image of the saint that the Spaniards brought to our country, leaving it in the first instance in Chillán. In 1655, when Mapuche clashed with Spanish soldiers, they hid the image in Yumbel, a village located 70 km away. of Concepción. When the conflicts ended, Chillán wanted the image back, but he did not succeed. Today, about 300 thousand people gather to worship her on their holiday.',
      color: "#e732c9",
      hover_color: "#b7159c",
      url: ""
    },
    CHL2703: {
      name: "Libertador General Bernardo O'Higgins",
      description: '<br><b>Inmaculada Concepcion: </b><br>'+
      'As in the celebration of the Virgin of Lo Vásquez in the region of Valparaíso, thousands of faithful arrive at the town of Graneros, on December 8, to pay homage to the Virgin, whose image is kept in the Church of the Treasury of the company.',
      color: "#1cd0fd",
      hover_color: "#02a2ca",
      url: ""
    },
    CHL2704: {
      name: "Los Lagos",
      description: '<br><b>Festival Internacional de Jazz en Puerto Montt:</b><br>'+
      'The International Jazz Festival of Puerto Montt is a jazz festival that since 2004 is held annually during the month of October in the city of Puerto Montt. This festival is the southernmost in Chile and one of the southernmost in the world in its kind. Each year it presents artists from different countries, which offer free concerts for the spectators.<br><br>'+
      '<b>Leche y Carne Osorno:</b><br>'+
      'The Festival and National Festival of Milk and Meat has earned for several to be recognized as the largest Festival in southern Chile. It is one of the main tourist events held in the province, having two central activities: As a festival, the event has numerous shows, those with musical artists, as well as the field of humor. In addition, it has its own competition of singer-songwriters and composers of folk songs. On the other hand, it has a great variety of cultural and recreational activities, such as artisanal exhibitions, parade of floats, a gastronomic sample, competition of roasted stick, among others.',
      color: "#1cd0fd",
      hover_color: "#02a2ca",
      url: ""
    },
    CHL2705: {
      name: "Maule",
      description: '<br><b>La Vendimia:</b> <br>'+
      'La Vendimia festival in the city of Curicó is the oldest of the celebrations that pay tribute to our wine, and begins with a religious ceremony to bless the first musts and give way to floats. The harvest chooses its queen, who is weighed in bottles of wine on a scale, while the grape trophy competition is held, with teams competing for 10 minutes until they turn into juice.',
      color: "#ffa71a",
      hover_color: "#cc7e00",
      url: ""
    }
  },
  locations: {
    "0": {
      lat: "-33.45",
      lng: "-70.666667",
      name: "Santiago"
    }
  }
};