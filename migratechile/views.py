from django.shortcuts import render
from django.http import HttpResponse
from .models import slang, pnumber, region, News
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail

def index(request):
    amusing = request.session.get('amusing',None)
    title = News.objects.all()
    return render(request, 'index.html', {'nbar':'index','amusing':amusing,'title':title})

def map(request):
    amusing = request.session.get('amusing',None)
    regions = region.objects.all()
    return render(request, 'map.html', {'nbar':'map','amusing':amusing,'regions':regions})

def slang_page(request):
    amusing = request.session.get('amusing',None)
    words = slang.objects.all()
    return render(request, 'slang.html', {'nbar':'slang','words':words,'amusing':amusing})

def contact(request):
    amusing = request.session.get('amusing',None)
    return render(request, 'contact.html', {'nbar':'contact','amusing':amusing})

def about(request):
    amusing = request.session.get('amusing',None)
    nmb = pnumber.objects.all()
    return render(request, 'about.html', {'nbar':'about','amusing':amusing,'nmb':nmb})

def add_slang(request):
    amusing = request.session.get('amusing',None)
    if amusing == 'admin':

        word = request.POST.get('word')
        meaning = request.POST.get('meaning')
        new_slang = slang(word = word, meaning = meaning)
        new_slang.save()
        return redirect('/slang/')
    else:
        return redirect('/slang/')

def delete_slang(request, id):
    amusing = request.session.get('amusing',None)
    if amusing == 'admin':
        e = slang.objects.get(id = id)
        e.delete()
        return redirect('/slang/')
    else:
        return redirect('/slang/')

def slang_page_edit(request, id):
    amusing = request.session.get('amusing',None)
    if amusing == 'admin':    
        word = slang.objects.get(id = id)
        return render(request, 'editslang.html',{'id':id,'word':word,'amusing':amusing})
    else:
        return redirect('/slang/')

def save_edit_slang(request, id):
    amusing = request.session.get('amusing',None)
    if amusing == 'admin':
        e = slang.objects.get(id = id)
        e.word = request.POST.get('word')
        e.meaning = request.POST.get('meaning')
        e.save()
        return redirect ('/slang/')
    else:
        return redirect ('/slang/')

def login(request):
    amusing = request.session.get('amusing',None)
    if amusing is not None:
        return redirect('index')
    else:
        return render(request, 'login.html',{'amusing':amusing})

def loginsuccess(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    au = authenticate(username = username, password = password)

    if au is not None:
        request.session['amusing'] = au.username
        return redirect('/')
    else:
        return redirect('login')

def logout(request):
    del request.session['amusing']
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def add_pnumber(request):
    amusing = request.session.get('amusing',None)
    if amusing == 'admin':
        name = request.POST.get('name')
        number = request.POST.get('number')
        new_pnumber = pnumber(name = name, number = number)
        new_pnumber.save()
        return redirect('/about/')
    else:
        return redirect('/about/')

def delete_pnumber(request, id):
    amusing = request.session.get('amusing',None)
    if amusing == 'admin':
        e = pnumber.objects.get(id = id)
        e.delete()
        return redirect('/about/')
    else:
        return redirect('/about/')

def pnumber_page_edit(request, id):
    amusing = request.session.get('amusing',None)
    if amusing == 'admin':    
        nmb = pnumber.objects.get(id = id)
        return render(request, 'editpnumber.html',{'id':id,'nmb':nmb,'amusing':amusing})
    else:
        return redirect('/about/')

def save_edit_pnumber(request, id):
    amusing = request.session.get('amusing',None)
    if amusing == 'admin':
        e = pnumber.objects.get(id = id)
        e.name = request.POST.get('name')
        e.number = request.POST.get('number')
        e.save()
        return redirect ('/about/')
    else:
        return redirect ('/about/')
       
def region_edit_page(request, code):
    amusing = request.session.get('amusing',None)
    if amusing == 'admin':    
        r = region.objects.get(code = code)
        return render(request, 'editregion.html',{'id':id,'r':r,'amusing':amusing})
    else:
        return redirect('/map/')
        
def save_edit_region(request, code):
    amusing = request.session.get('amusing',None)
    if amusing == 'admin':
        r = region.objects.get(code = code)
        r.name = request.POST.get('name')
        r.description = request.POST.get('description')
        r.color = request.POST.get('color')
        r.hover_color = request.POST.get('hover_color')
        r.url = request.POST.get('url')
        r.save()
        return redirect ('/map/')
    else:
        return redirect ('/map/')


def add_new(request):
    amusing = request.session.get('amusing',None)
    if amusing == 'admin':

        title = request.POST.get('title')
        description = request.POST.get('description')
        link = request.POST.get('link')
        new_news = News(title = title, description = description, link = link)
        new_news.save()
        return redirect('../')
    else:
        return redirect('../')

def delete_new(request, id):
    amusing = request.session.get('amusing',None)
    if amusing == 'admin':
        n = News.objects.get(id = id)
        n.delete()
        return redirect('../../')
    else:
        return redirect('../../')

def new_page_edit(request, id):
    amusing = request.session.get('amusing',None)
    if amusing == 'admin':    
        nws = News.objects.get(id = id)
        return render(request, 'editnews.html',{'id':id,'nws':nws,'amusing':amusing})
    else:
        return redirect('../../../')

def save_edit_new(request, id):
    amusing = request.session.get('amusing',None)
    if amusing == 'admin':
        n = News.objects.get(id = id)
        n.title = request.POST.get('title')
        n.description = request.POST.get('description')
        n.link = request.POST.get('link')
        n.save()
        return redirect ('../../../')
    else:
        return redirect ('../../../')

def sendmail(request):
    amusing = request.session.get('amusing',None)

    _subject = request.POST.get('name') + ' - ' + request.POST.get('email')
    _message = request.POST.get('message')
    _from = 'migratechileitfair@gmail.com'
    _to = 'migratechileitfair@gmail.com'
    send_mail(_subject, _message, _from, [_to], fail_silently=False)
    return render(request, 'sent.html',{'amusing':amusing})