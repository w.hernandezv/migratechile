"""migratechile URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name='index'),
    path('map/', views.map, name='map'),
    path('slang/', views.slang_page, name='slang_page'),
    path('contact/', views.contact, name='contact'),
    path('about/', views.about, name='about'),
    path('slang/add', views.add_slang, name='add_slang'),
    path('slang/edit/<int:id>', views.slang_page_edit, name='slang_page_edit'),
    path('slang/delete/<int:id>', views.delete_slang, name='delete_slang'),
    path('slang/edit/<int:id>/save', views.save_edit_slang, name='save_edit_slang'),
    path('login/', views.login, name="login"),
    path('login/success/', views.loginsuccess, name="loginsuccess"),
    path('logout/', views.logout, name="logout"),
    path('about/add', views.add_pnumber, name='add_pnumber'),
    path('about/edit/<int:id>', views.pnumber_page_edit, name='pnumber_page_edit'),
    path('about/delete/<int:id>', views.delete_pnumber, name='delete_pnumber'),
    path('about/edit/<int:id>/save', views.save_edit_pnumber, name='save_edit_pnumber'),
    path('map/edit/<str:code>', views.region_edit_page, name='region_edit_page'),
    path('map/edit/<str:code>/save', views.save_edit_region, name='save_edit_region'),
    path('new/add', views.add_new, name='add_new'),
    path('new/edit/<int:id>', views.new_page_edit, name='new_page_edit'),
    path('new/delete/<int:id>', views.delete_new, name='delete_new'),
    path('new/edit/<int:id>/save', views.save_edit_new, name='save_edit_new'),
    path('contact/send', views.sendmail, name='sendmail'),
]
