from django.db import models

class slang(models.Model):
    word = models.CharField(max_length=50)
    meaning = models.CharField(max_length=200)

    def __str__(self):
        return self.word

class pnumber(models.Model):
    name = models.CharField(max_length=200)
    number = models.IntegerField()

class region(models.Model):
    code = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=1500)
    color = models.CharField(max_length=100)
    hover_color = models.CharField(max_length=100)
    url = models.CharField(max_length=300)

    def __str__(self):
        return self.name

class News(models.Model):
    title = models.CharField(max_length=100)
    description = models.CharField(max_length=1500)
    link = models.CharField(max_length=300)

    def __str__(self):
        return self.title