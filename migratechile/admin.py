from django.contrib import admin

from .models import slang, pnumber, region, News

admin.site.register(slang)
admin.site.register(pnumber)
admin.site.register(region)
admin.site.register(News)